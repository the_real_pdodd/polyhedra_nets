"""
Store and retrieve data from the data base.

"""
import pandas as pd
from datetime import datetime, timedelta
import pymongo
import signac
import logging
logger = logging.getLogger("polyhedra_nets.{}".format(__name__));

host='glotzerdb';
dbname = 'polyhedra_nets';
nets_test_collection = 'nets_testing';
nets_collection = 'nets';

def verify_doc(doc):
    return False;

def test_connection():
    connected = True;
    try:
        db = signac.get_database(name=dbname, hostname=host);
        if glotzq_collection not in db.collection_names():
            print("Could not find {!r} in {!r}".format(glotzq_collection, dbname))
            connected = False;
        if glotzq_test_collection not in db.collection_names():
            print("Could not find {!r} in {!r}".format(glotzq_test_collection, dbname))
            connected = False;
    except:
        connected = False;
    return connected;

def upload(entry, collection=glotzq_collection):
    """
    http://api.mongodb.com/python/current/api/pymongo/collection.html
    """
    db = signac.get_database(name=dbname, hostname=host);
    db[collection].insert_one(entry);

def retrieve_latest(account, collection=glotzq_collection, limit=1):
    """
    Retrive the last database entry based on the time and account
    """
    db = signac.get_database(name=dbname, hostname=host);
    yield None

def find_entries(collection=nets_collection, filter=None):
    """
    Retrive all entries associated with the account
    """
    db = signac.get_database(name=dbname, hostname=host);
    for doc in db[collection].find(filter=filter):
        yield doc

def list_entries(account, collection=glotzq_collection, sort=False, limit=0):
    db = signac.get_database(name=dbname, hostname=host);
    stats = db.command("collstats", collection, scale=1024*1024)
    logger.warning("Reporting information for {ns}".format(ns=stats['ns']))
    logger.warning("entries found: {entries}\ntotal size: {sz:.1f} MB\naverage size: {asz:.1f} KB".format(entries=stats['count'], sz=stats['size'], asz=1024*stats['size']/stats['count']))
    # if sort:
    #     sort = [('time', pymongo.ASCENDING)];
    # else:
    #     sort=None
    # for doc in db[collection].find(filter={'account':account}, limit=limit, sort=sort):
    #     logger.warning("{date} {acc} {jobs}".format(date=datetime.fromtimestamp(doc['time']).strftime('%c'), acc=doc['account'], jobs=len(doc['jobs']['User'])))

def purge_entries(collection=glotzq_collection):
    pass;

def format(*args, **kwargs):
    return dict(
                **kwargs
                );

def get_database():
    return signac.get_database(name=dbname, hostname=host);
