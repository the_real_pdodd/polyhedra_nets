
from . import database
from .database import get_database, upload, find_entries, collection_stats, list_all

def test():
    return database.test_connection();

__all__  = [
    'test',
    'database'
]
