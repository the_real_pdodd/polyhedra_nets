import sys
from setuptools import setup, find_packages

if sys.version_info < (2, 7, 0):
    print("Error: foldingdb requires python version >= 2.7.x.")
    sys.exit(1)

setup(
    name='polyhedra_nets',
    version='0.0.0',
    packages=find_packages(),
    zip_safe=True,
    author='Paul M Dodd',
    author_email='pdodd@umich.edu',
    description="database for polyhedra nets",
    url="https://the_real_pdodd@bitbucket.org/the_real_pdodd/polyhedra_nets.git",
    install_requires=['pandas', 'pymongo', 'signac'], #  install_requires or something else?
)
